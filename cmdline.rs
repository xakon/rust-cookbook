//
// cmdline.rs
// Print command-line arguments, first required, second optional.
//

use std::env;


fn main() {
   for arg in env::args() {
      println!("{}", arg);
   }

   let (input, output) = common_way();
   println!("reading from {}, writing to {}", input, output);

   let (input, output) = rust_way();
   println!("reading from {}, writing to {}", input, output);
}

fn common_way() -> (String, String) {
   let mut arg = env::args();
   let input = arg.nth(1).expect("missing expected input");
   let output = arg.nth(0).unwrap_or("STDOUT".to_string());
   (input, output)
}

fn rust_way() -> (String, String) {
   let mut args = env::args().skip(1).take(2);
   let input = args.next().expect("missing expected input");
   let output = args.next().unwrap_or("STDOUT".to_string());
   (input, output)
}
