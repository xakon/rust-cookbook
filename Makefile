##
## Makefile
## General rules for building basic, Rust programs
##

SHELL		:= /bin/sh

#RUSTFLAGS	+= -Wall -Wextra
RUSTCC		?= rustc

SOURCES		:= $(wildcard *.rs)
TARGETS		:= ${SOURCES:.rs=}

all: help
help:
	@echo 'build/manage misc Rust programs'
	@echo
	@echo 'available rules are:'
	@echo '   help      - this message'
	@echo '   build     - build defined programs'
	@echo '   test      - run any available tests'
	@echo '   clean     - remove object files'
	@echo '   cleanall  - remove all generated files'
	@echo '   veryclean - remove all generated files'
clean:
	-${RM} *.o
veryclean: cleanall
cleanall: clean
	-${RM} ${TARGETS}
build: ${TARGETS}
test:
	@echo 'nothing to test for now'


##
## Manual dependencies
##

##
## Custom rules
##

%: %.rs
	${RUSTCC} ${RUSTFLAGS} $<

.PHONY: all help build clean veryclean cleanall test
